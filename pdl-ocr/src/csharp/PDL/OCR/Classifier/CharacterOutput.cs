﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR.Classifier
{
    public class CharacterOutput
    {
        public char Result
        {
            get;
            set;
        }

        public FontInfo Font
        {
            get;
            set;
        }

        public double Likelihood
        {
            get;
            set;
        }
    }
}
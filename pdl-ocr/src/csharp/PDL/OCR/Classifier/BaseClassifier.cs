﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR.Classifier
{
    public abstract class BaseClassifier
    {
        /// <summary>
        /// a set of fonts that are candidates for the scanned text. set this property to null
        /// to indicate that all available fonts (and styles) will be used.
        /// </summary>
        public IList<FontInfo> Fonts
        {
            get;
            set;
        }

        public CharacterOutput Classify(Character character)
        {
            return Classify(character, 1)[0];
        }

        /// <summary>
        /// returns the top K likely characters (with their individual probabilities).
        /// </summary>
        /// <param name="character"></param>
        /// <param name="topK"></param>
        /// <returns></returns>
        public abstract IList<CharacterOutput> Classify(Character character, int topK);
    }
}
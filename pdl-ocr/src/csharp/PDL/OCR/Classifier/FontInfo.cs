﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR.Classifier
{
    public class FontInfo
    {
        public string Name
        {
            get;
            set;
        }

        public int Size
        {
            get;
            set;
        }

        public bool Bold
        {
            get;
            set;
        }

        public bool Italic
        {
            get;
            set;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR
{
    public class Word
    {
        public Line Line
        {
            get;
            set;
        }

        public int Left
        {
            get;
            set;
        }

        public int Right
        {
            get;
            set;
        }

        public int Top
        {
            get;
            set;
        }

        public int Bottom
        {
            get;
            set;
        }

        public IList<Character> Characters
        {
            get;
            set;
        }
    }
}

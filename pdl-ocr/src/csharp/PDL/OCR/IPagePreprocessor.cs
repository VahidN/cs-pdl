﻿
namespace PDL.OCR
{
    /// <summary>
    /// This interface defines the functions that are to be applied to an image before it
    /// is sent to the layout analyzer.
    /// </summary>
    public interface IPagePreprocessor
    {
        /// <summary>
        /// Find the skew angle of the page and rotate the image so that the lines are horizontal.
        /// 
        /// (Manipulates page.Image directly.)
        /// </summary>
        /// <param name="page"></param>
        void Orient(Page page);

        /// <summary>
        /// Removes noise from the image.
        /// </summary>
        /// <param name="page"></param>
        void Denoise(Page page);
    }
}
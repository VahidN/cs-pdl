﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR
{
    public class Character
    {
        public Word Word
        {
            get;
            set;
        }

        public int Left
        {
            get;
            set;
        }

        public int Right
        {
            get;
            set;
        }

        public int Top
        {
            get;
            set;
        }

        public int Bottom
        {
            get;
            set;
        }
    }
}
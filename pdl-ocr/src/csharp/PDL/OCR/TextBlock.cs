﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR
{
    public class TextBlock : Block
    {
        public override BlockType Type
        {
            get { return BlockType.Text; }
        }

        public IList<Line> Lines
        {
            get;
            set;
        }
    }
}
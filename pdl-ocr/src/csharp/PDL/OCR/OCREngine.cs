﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    public class OCREngine
    {
        private readonly ILayoutAnalyzer layoutAnalyzer;

        private readonly IPagePreprocessor pagePreprocessor;

        private readonly ITextExtractor extractor;

        public OCREngine(IPagePreprocessor preprocessor, ILayoutAnalyzer analyzer, ITextExtractor extractor)
        {
            this.layoutAnalyzer = analyzer;
            this.pagePreprocessor = preprocessor;
            this.extractor = extractor;
        }

        public IList<string> Recognize(Image image, SegmentationMode mode)
        {
            // NOTE: rough procedure only

            Page page = new Page();
            page.Image = image;
            // TO-DO: might need to fill in additional properties for the page

            // preprocess: orient & denoise
            if (mode == SegmentationMode.Page)
            {
                pagePreprocessor.Orient(page);
                pagePreprocessor.Denoise(page);
            }

            // detect layout & segment
            layoutAnalyzer.Analyze(page, mode);

            // detect text
            List<string> result = new List<string>();
            foreach (Block block in page.Blocks)
            {
                if (block is TextBlock)
                {
                    result.AddRange(extractor.ExtractText(block as TextBlock));
                }
            }

            return result;
        }
    }
}
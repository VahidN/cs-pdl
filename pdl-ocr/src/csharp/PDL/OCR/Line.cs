﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    public class Line
    {
        public TextBlock Block
        {
            get;
            set;
        }

        public int Left
        {
            get;
            set;
        }

        public int Right
        {
            get;
            set;
        }

        public int Top
        {
            get;
            set;
        }

        public int Bottom
        {
            get;
            set;
        }

        public IList<Word> Words
        {
            get;
            set;
        }
    }
}

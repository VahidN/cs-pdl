﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PDL.OCR.Classifier;

namespace PDL.OCR.Impl
{
    /// <summary>
    /// </summary>
    public class DefaultTextExtractor : ITextExtractor
    {
        //private BaseClassifier classifier = null;

        public IWordEngine WordEngine
        {
            get;
            set;
        }

        public DefaultTextExtractor(IWordEngine wordEngine)
        {
            this.WordEngine = wordEngine;
        }

        public virtual IList<string> ExtractText(TextBlock block)
        {
            // just a rough algorithm illustrating how this extractor might work;
            // depending on the method used, this procedure might need to be modified

            // 1. run classifer for each character in the block, using all fonts;
            // 2. determine the most likely 1, 2 or 3 fonts (say normal, bold, and italic), set
            //    the font info of the classifer;
            // 3. re-run classifer for each character in the block, using restricted set of fonts;
            // 4. for each word in the block, since we have each character's confidence level,
            //    simply selects the word with the highest probability according to some scoring formula;

            throw new NotImplementedException();
        }
    }
}

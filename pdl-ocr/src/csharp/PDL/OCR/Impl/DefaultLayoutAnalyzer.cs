﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR.Impl
{
    public class DefaultLayoutAnalyzer : ILayoutAnalyzer
    {
        public void Analyze(Page page, SegmentationMode mode)
        {
            page.Blocks = new List<Block>();

            // parses the page into blocks (text, non-text)
            // TO-DO

            // segments each text block into lines
            foreach (Block block in page.Blocks)
            {
                if (block is TextBlock)
                {
                    Segment(block as TextBlock);
                }
            }
        }

        public void Segment(TextBlock block)
        {
            // block -> lines, lines -> words, word -> characters
            // we need some private helper methods
            throw new NotImplementedException();
        }
    }
}
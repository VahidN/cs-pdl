﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    /// <summary>
    /// This interface defines methods that analyzes the layout of the page (column, line, character).
    /// </summary>
    public interface ILayoutAnalyzer
    {
        /// <summary>
        /// Parses a page into a list of blocks, each can be text block or non-text block.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="mode"></param>
        void Analyze(Page page, SegmentationMode mode);
    }
}
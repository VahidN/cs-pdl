﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    /// <summary>
    /// Represents a block in the layout of a page.
    /// </summary>
    public abstract class Block
    {
        public Page Page
        {
            get;
            set;
        }

        public int Left
        {
            get;
            set;
        }

        public int Right
        {
            get;
            set;
        }

        public int Top
        {
            get;
            set;
        }

        public int Bottom
        {
            get;
            set;
        }

        public abstract BlockType Type
        {
            get;
        }
    }
}
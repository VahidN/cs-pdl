﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    public class Page
    {
        public Image Image
        {
            get;
            set;
        }

        public IList<Block> Blocks
        {
            get;
            set;
        }
    }
}
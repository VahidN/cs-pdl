﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDL.OCR
{
    /// <summary>
    /// Defines methods that extract text from segmented data.
    /// </summary>
    public interface ITextExtractor
    {
        /// <summary>
        /// Gets or sets the word engine used by the text extractor.
        /// </summary>
        IWordEngine WordEngine
        {
            get;
            set;
        }

        /// <summary>
        /// Extracts text from a text block.
        /// </summary>
        /// <param name="block"></param>
        /// <returns></returns>
        IList<string> ExtractText(TextBlock block);
    }
}
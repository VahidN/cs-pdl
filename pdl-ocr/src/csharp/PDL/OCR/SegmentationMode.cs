﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDL.OCR
{
    /// <summary>
    /// Represents the possible segmentation modes
    /// </summary>
    public enum SegmentationMode
    {
        Character,

        Word,

        Line,

        Block,

        Page
    }
}
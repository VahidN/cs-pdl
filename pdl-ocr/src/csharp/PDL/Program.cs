﻿using System;
using System.Collections.Generic;
using System.Text;

using PDL.OCR;
using PDL.OCR.Impl;
using PDL.OCR.Util;

namespace PDL
{
    class Program
    {
        static void Main(string[] args)
        {
            // demonstrates one possible usage of the system

            OCREngine engine = new OCREngine(
                new DefaultPagePreprocessor(),
                new DefaultLayoutAnalyzer(),
                new DefaultTextExtractor(new EnglishWordEngine()));

            Image image = FileUtil.LoadImage("sample.tiff");
            IList<string> text = engine.Recognize(image, SegmentationMode.Page);
        }
    }
}
